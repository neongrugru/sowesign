#!/usr/bin/env bash
RESULT=`mysql -u marie -pathena --skip-column-names -e "SHOW DATABASES LIKE 'sowesign'"`
if [ "$RESULT" == "sowesign" ]; then
    printf "$(tput setaf 7)$(tput setab 4)Dropping database...$(tput sgr 0) \n"
    php bin/console doctrine:database:drop --force
    printf "$(tput setaf 0)$(tput setab 2)Database dropped$(tput sgr 0) \n\n"
    printf "$(tput setaf 7)$(tput setab 4)Creating database...$(tput sgr 0) \n"
    php bin/console doctrine:database:create
    printf "$(tput setaf 0)$(tput setab 2)Database created$(tput sgr 0) \n\n"
else
    printf "$(tput setaf 7)$(tput setab 4)Creating database...$(tput sgr 0) \n"
    php bin/console doctrine:database:create
    printf "$(tput setaf 0)$(tput setab 2)Database created$(tput sgr 0) \n\n"
fi
    printf "$(tput setaf 7)$(tput setab 4)Updating database schema...$(tput sgr 0) \n"
    php bin/console doctrine:schema:update --dump-sql --force
    printf "$(tput setaf 0)$(tput setab 2)Database schema updated$(tput sgr 0) \n\n"