<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(
     *     pattern     = "/^[a-zA-Z'. -]+$/i",
     *     htmlPattern = "^[a-zA-Z'. -]+$",
     *     match=false,
     *     message="Mauvais format du prénom"
     * )
    */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(
     *     pattern     = "/^[a-zA-Z'. -]+$/i",
     *     htmlPattern = "^[a-zA-Z'. -]+$",
     *     match=false,
     *     message="Mauvais format du nom"
     * )
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $company;

    /**
     * @ORM\Column(type="boolean")
     */
    private $hassigned;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isadmin;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Rds", inversedBy="users")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rds;

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getHassigned(): ?bool
    {
        return $this->hassigned;
    }

    public function setHassigned(bool $hassigned): self
    {
        $this->hassigned = $hassigned;

        return $this;
    }

    public function getIsadmin(): ?bool
    {
        return $this->isadmin;
    }

    public function setIsadmin(bool $isadmin): self
    {
        $this->isadmin = $isadmin;

        return $this;
    }

    public function getRds(): ?Rds
    {
        return $this->rds;
    }

    public function setRds(?Rds $rds): self
    {
        $this->rds = $rds;

        return $this;
    }
}
