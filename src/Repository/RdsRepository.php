<?php

namespace App\Repository;

use App\Entity\Rds;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Rds|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rds|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rds[]    findAll()
 * @method Rds[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RdsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Rds::class);
    }

//    /**
//     * @return Rds[] Returns an array of Rds objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Rds
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
