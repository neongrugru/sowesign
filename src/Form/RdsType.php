<?php

namespace App\Form;

use App\Entity\Rds;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;

class RdsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pdf', FileType::class, [
                'label' => 'Document pdf'
            ])
            ->add('topic', null, [
                'label' => false,
                'translation_domain'    => 'messages',
                'attr'  => [
                    'placeholder'   => 'form.rds.topic',
                ]
            ])
            ->add('description', null, [
                'label' => false,
                'translation_domain'    => 'messages',
                'attr'  => [
                    'placeholder'   => 'form.rds.desc',
                ]
            ])
            ->add('dateFrom', TextType::class, [
                'label'     => false,
                'attr'      => [
                    'class'     => 'c__step-form-date-elem js_date-picker-start',
                    'placeholder'   => 'form.rds.datestart',
                ]
            ])
            ->add('dateTo', TextType::class, [
                'label'     => false,
                'attr'      => [
                    'class'         => 'c__step-form-date-elem js_date-picker-stop',
                    'placeholder'   => 'form.rds.datestop',
                ]
            ])
            ->add('link', UrlType::class, [
                'label' => false,
                'translation_domain'    => 'messages',
                'attr'  => [
                    'placeholder'   => 'form.rds.link',
                ]
            ])
            ->add('adminLink')
            ->add('open')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Rds::class,
        ]);
    }
}
