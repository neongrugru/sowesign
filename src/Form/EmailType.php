<?php

namespace App\Form;

use App\Entity\Email;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class EmailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('topic', null, [
                'label' => false,
                'translation_domain'    => 'messages',
                'attr'  => [
                    'placeholder'   => 'form.rds.sujet',
                ]
            ])
            ->add('content', null, [
                'label' => false,
                'translation_domain'    => 'messages',
                'attr'  => [
                    'placeholder'   => 'form.rds.message',
                ]
            ])
            ->add('reminder', null,  [
                'label' => false,
                'translation_domain'    => 'messages',
                'attr'  => [
                    'placeholder'   => 'form.rds.relance',
                ]
            ])
            ->add('frequency', NumberType::class, [
                'label' => false,
                'translation_domain'    => 'messages',
                'attr'  => [
                    'placeholder'   => 'form.rds.frequence',
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Terminer',
                'attr' => [
                    'class' => 'save'
                ]
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Email::class,
        ]);
    }
}
