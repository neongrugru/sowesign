<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class SignataireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('name', null, [
            'label' => false,
            'translation_domain'    => 'messages',
            'attr'  => [
                'placeholder'   => 'form.rds.nameSign',
                'minlength'     => 2
            ]
        ])
        ->add('surname', null, [
            'label' => false,
            'translation_domain'    => 'messages',
            'attr'  => [
                'placeholder'   => 'form.rds.surnameSign',
                'minlength'     => 2
            ]
        ])
        ->add('email', EmailType::class, [
            'label' => false,
            'translation_domain'    => 'messages',
            'attr'  => [
                'placeholder'   => 'form.rds.emailSign',
            ]
        ])
        ->add('company', null, [
            'label' => false,
            'translation_domain'    => 'messages',
            'attr'  => [
                'placeholder'   => 'form.rds.companySign',
            ]
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
