<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class AdminType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => false,
                'translation_domain'    => 'messages',
                'attr'  => [
                    'placeholder'   => 'form.rds.name',
                    'minlength'     => 2
                ]
            ])
            ->add('surname', null, [
                'label' => false,
                'translation_domain'    => 'messages',
                'attr'  => [
                    'placeholder'   => 'form.rds.surname',
                    'minlength'     => 2
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => false,
                'translation_domain'    => 'messages',
                'attr'  => [
                    'placeholder'   => 'form.rds.email',
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
