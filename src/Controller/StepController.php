<?php

namespace App\Controller;

use App\Entity\Rds;
use App\Entity\User;
use App\Entity\Email;
use App\Form\AdminType;
use App\Form\RdsType;
use App\Form\SignataireType;
use App\Form\EmailType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class StepController extends Controller
{
    /**
     * @Route("/step/{name}", name="step")
     */
    public function index($name = null)
    {
        $analyse = new AnalyseController();
        $response = json_decode($analyse->index($name));
        $rds = new Rds();
        $admin = new User();
        $signataire = new User();
        $email = new Email();

        if ($response){

            if($usersresponse = $response->users) {
                foreach ($usersresponse as $key => $user) {
                    array_shift($user);
                    $n_user = new User();
                    $n_user->setName($user['0']);
                    $n_user->setSurname($user['1']);
                    $n_user->setCompany($user['2']);
                    $n_user->setEmail($user['3']);
                    $n_form = $this->createForm(SignataireType::class, $n_user);
                    $formSignataires[$key] = $n_form->createView();
                }
            }

            if($adminResponse = $response->admin) {
                array_shift($adminResponse);
                $admin->setName($adminResponse['1']);
                $admin->setSurname($adminResponse['0']);
                $admin->setEmail($adminResponse['2']);
            }

            if($emailResponse = $response->retry) {
                array_shift($emailResponse);
                $email->setFrequency($emailResponse['0']);
                $email->setReminder($emailResponse['1']);
            }

            if($scheduleResponse = $response->schedule){
                array_shift($scheduleResponse);
            }

            if($meetingResponse = $response->meeting){
                array_shift($meetingResponse);
                $email->setTopic($meetingResponse['0']);
                $email->setContent($meetingResponse['1']);
            }
        }

        $formRds = $this->createForm(RdsType::class, $rds);
        $formAdmin = $this->createForm(AdminType::class, $admin);
        $formSignataire = $this->createForm(SignataireType::class, $signataire);
        $formEmail = $this->createForm(EmailType::class, $email);
        return $this->render('step/index.html.twig', [
            'controller_name'   => 'StepController',
            'formAdmin'         => $formAdmin->createView(),
            'formRds'           => $formRds->createView(),
            'formSignataire'    => $formSignataire->createView(),
            'formEmail'         => $formEmail->createView(),
            'formsSignataires'  => $formSignataires,
        ]);
    }
}
