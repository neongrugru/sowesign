<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->render('index/index.html.twig', [
            'controller_name' => 'IndexController',
        ]);
    }

    /**
     * @Route("/upload", name="upload")
     */
    public function upload(Request $request)
    {
        $file = $_FILES;
        $name = $file['file-field']['name'];
        $tmp_name = $file['file-field']['tmp_name'];
        $moveFile = move_uploaded_file($tmp_name, "upload/$name");

        if($moveFile) {
            return $this->redirectToRoute('step', array('name' => $name));
        } else {
            return $this->redirectToRoute('index');
        }
    }
}
