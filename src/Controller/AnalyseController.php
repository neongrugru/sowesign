<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use PDFLib\PDFLib;

class AnalyseController extends Controller
{
    /**
     * @Route("/analyse/{name}", name="analyse")
     */
    public function index($name = null)
    {
        $parser = new \Smalot\PdfParser\Parser();
        $filename = 'upload/'.$name;
        $pdf    = $parser->parseFile($filename);

        $datas = array();

        if($pdf) {
            $text  = preg_replace('/\s+/', '', $pdf->getText());
            preg_match_all('#\#SIGN[0-9]{3}\##', $text, $signers, PREG_OFFSET_CAPTURE);
            preg_match_all('#\#USER[0-9]{3}\#((([^\#]*))\#){4}#', $text, $users, PREG_OFFSET_CAPTURE);
            preg_match_all('#\#ADMIN\#((([^\#]*))\#){3}#', $text, $datas['admin'], PREG_OFFSET_CAPTURE);
            preg_match_all('#\#RETRY\#((([^\#]*))\#){3}#', $text, $datas['retry'], PREG_OFFSET_CAPTURE);
            preg_match_all('#\#SCHEDULE\#((([^\#]*))\#){4}#', $text, $datas['schedule'], PREG_OFFSET_CAPTURE);
            preg_match_all('#\#MEETING\#((([^\#]*))\#){2}#', $text, $datas['meeting'], PREG_OFFSET_CAPTURE);

            foreach ($datas as $key => $data) {
                $temp = explode('#', $data[0][0][0]);
                array_shift($temp);
                array_pop($temp);
                $datas[$key] = $temp;
            }
            foreach($signers[0] as $key => $sign){
                $temp = explode('#', $sign[0]);
                array_shift($temp);
                array_pop($temp);
                $datas['signs'][$key] = $temp;
            }
            foreach($users[0] as $key => $user){
                $temp = explode('#', $user[0]);
                array_shift($temp);
                array_pop($temp);
                $datas['users'][$key] = $temp;
            }

        } else {
            $text = "fichier non trouvé";
        }

        return json_encode($datas);
    }
}
