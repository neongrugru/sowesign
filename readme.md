# Install sowesign project

#### Clone the repo
> git clone git@gitlab.com:neongrugru/sowesign.git

#### Install the project
> cd sowesign

> composer update

> npm install

> yarn run encore dev

#### Create database
Edit the .env file and then run
> php bin/console doctrine:database:create

### Updating database schema
> php bin/console doctrine:schema:update --force

#### En dev
Run server :

> php bin/console server:start

# Enjoy sowewign !
