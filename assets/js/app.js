import TinyPicker from 'tiny-picker';

const imagesContext = require.context('../img', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);

let steps = require('./steps');
let modal = require('./modal');
let analyse = require('./analyse');
let dragAndDrop = require('./drag-and-drop');
let currentPath = window.location.pathname;

window.addEventListener('load', function () {
    if (currentPath === '/') {
        modal();
        dragAndDrop();
    }

    if (currentPath === '/step') {
        steps();
        new TinyPicker({
            firstBox: document.getElementsByClassName('js_date-picker-start')[0],
            lastBox: document.getElementsByClassName('js_date-picker-stop')[0],
            monthsToShow: 2,
            days: ['Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim'],
            local: 'fr-FR',
        }).init();
    }

    if(currentPath === '/analyse') {
        analyse();
    }
});