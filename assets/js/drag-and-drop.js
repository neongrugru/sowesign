module.exports = function() {

    document.getElementById('drop-zone').addEventListener('drop', function(event) {
        event.preventDefault();
        let uploadCircle = document.getElementById('upload-circle');
        let fileInput = document.getElementById('file-field');
        uploadCircle.style.border = '20px solid green'; 

        const droppedFile = event.dataTransfer.files[0];
        fileInput.files = event.dataTransfer.files;
        
        if(checkFile(droppedFile.name)) {
            document.getElementById('upload').submit();
        }
    });

    document.getElementById('file-field').addEventListener('change', function(event) {
        event.preventDefault();
        const file = document.getElementById('file-field').files[0];
        if(checkFile(file.name)) {
            document.getElementById('upload').submit();
        }
    });

    document.getElementById('drop-zone').addEventListener('dragover', function(event) {
        event.preventDefault();
        let uploadCircle = document.getElementById('upload-circle');
        uploadCircle.style.border = '20px solid green';
    });

    document.getElementById('drop-zone').addEventListener('dragleave', function(event) {
        event.preventDefault();
        let uploadCircle = document.getElementById('upload-circle');
        uploadCircle.style.border = '20px solid #dedede';
    });

    function checkFile(file) {
        let extension = file.substr((file.lastIndexOf('.') +1));
        console.log(extension);
        if (!/(pdf)$/ig.test(extension)) {
            alert("Invalid file type: "+extension+".  Please use a PDF file.");
            return false;
        } else {
            return true;
        }
    }
};
