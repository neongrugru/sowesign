module.exports = function () {
    let hint = document.getElementsByClassName('js-upload-hint');
        hint = hint[0];
    let modal = document.getElementsByClassName('js-modal');
        modal = modal[0];
    let close = document.getElementsByClassName('js-modal-close');
        close = close[0];

    // Open hint modal
    hint.addEventListener('click', function () {
        if (modal.offsetHeight === 0) {
            modal.style.display = 'block';
        }
    });

    // Close hint modal
    close.addEventListener('click', function () {
        if (modal.style.display === 'block') {
            modal.style.display = 'none';
        }
    })
};
